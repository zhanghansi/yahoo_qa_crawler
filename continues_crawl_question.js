var _ = require('lodash');
var q = require('vo');
var fs= require('fs');
var Nightmare = require('nightmare');
var nightmare = Nightmare({
  show: false,
  gotoTimeout:  10000, // in ms
  loadTimeout:  10000,
  executionTimeout: 15000 // in ms
});


// every fixed time interval collect new links and push then in a file (10 links per file )
var program = require('commander');
program
//    .option('-c, --category <category>', 'goto : https://answers.yahoo.com/dir/index/answer?sid=396545018')
    .option('-p, --path <path>', 'data export path') // ./../../yahoo_qa/
    .parse(process.argv);

var root_path = '/mnt/data2/hansi.zhang/yahoo_qa_crawler/'
var cnt = 0;
var category = [
                      "cancer",
                      "alternative_medicine",
                      "dental",
                      "diet_fitness",
                      "allergies",
                      "diabetes",
                      "heart_diseases",
                      "infectious_diseases",
                      "other_diseases",
                      "respiratory_diseases",
                      "stds",
                      "skin_conditions",
                      "first_aid",
                      "injuries",
                      "other_general_health_care",
                      "pain",
                      "men_health",
                      "mental_health",
                      "women_health",
                      "other_health",
                      "optical"
                    ];




var run = function * () {
    var today = new Date()
    //console.log(program.path)
      /**** Crawl a new category  *****/
    // fs.openSync(path, 'a');
    // var content = fs.readFileSync(path, 'utf8')
    // if(content == '') {
    //   fs.writeFileSync(path,'[]','utf8');
    // }
    /******* end *******/
    var translator = JSON.parse(fs.readFileSync(root_path + 'translator.json', 'utf8'));

    //var question_link = 'https://answers.yahoo.com/dir/index/answer?sid='+category[program.category];
    var finish_scroll = false;
    var new_links = [];
    //console.log(check_links.length);
    yield nightmare.useragent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36")
                      .goto("https://answers.yahoo.com/dir/index/answer?sid=396545018")
                      .refresh()
                      .inject('js', root_path + 'node_modules/jquery/dist/jquery.js')
                      .wait(2000)
                      .exists('#ya-answer-tab > li')
                      .then(function(success) {
                          if(!success) {
                              nightmare.refresh()
                                       .inject('js', root_path + 'node_modules/jquery/dist/jquery.js')
                                       .wait(2000);
                          }
                      });

    console.log("crawl Date : "+ today);
    

    var pre_url_length = 0;
    do {
        var url = yield nightmare.evaluate(function() {
            var url = [];
            $('#ya-answer-tab > li').each(function(){
                var link_info = {};
                link_info['link'] = $(this).find('.qHead a').prop('href');
                link_info['ctg'] = $(this).find('.Fz-12.Clr-888 a').text();
                if(link_info['ctg'] != '' && link_info['link'] != null) {
                    url.push(link_info);
                }
            });
            return url;
        });
        //console.log(url[url.length-1]);
        //console.log(translator[url[url.length-1]['ctg']])
        var check_links = JSON.parse(fs.readFileSync(program.path + '/' + translator[url[url.length-1]['ctg']] + '/question/questions.json', 'utf8'));
        // console.log(check_links);
        if(check_links.indexOf(url[url.length-1]['link']) != -1 || pre_url_length == url.length) {
        // if(check_links.indexOf(url[url.length-1]['link']) != -1 ) {
            finish_scroll = true;
            new_links = url;
        } else {
              var currentHeight = yield nightmare.evaluate(function() {
                return document.body.scrollHeight;
              });
              yield nightmare.scrollTo(currentHeight, 0)
                .wait(5000);
        }
        pre_url_length = url.length;
        console.log(url.length);
    } while (!finish_scroll);
    console.log('start adding new links to all categories...');
    for(var j = 0; j < category.length; j++) {
        //var final_result = [];
        var check_links = JSON.parse(fs.readFileSync(program.path + '/' + category[j] + '/question/questions.json', 'utf8'));
        console.log('category : ' + category[j]);
        console.log('original length: ' + check_links.length);
        for(var i = 0; i < new_links.length; i ++) {
          if((translator[new_links[i]['ctg']] == category[j]) && (check_links.indexOf(new_links[i]['link']) == -1)) {
              check_links.push(new_links[i]['link']);
          }
        }
        //  Check null value and duplicate
        // for(var k = 0; k < check_links.length; k++) {
        //   if(check_links[k] != null && (final_result.indexOf(check_links[k]) == -1)) {
        //     final_result.push(check_links[k])
        //   }
        // }
        console.log('finished length: ' + check_links.length);
        fs.writeFileSync( program.path + '/' + category[j] + '/question/questions.json', JSON.stringify(check_links), 'utf8', function(err){
              if (err) throw err;
              console.log( category[j] + 'new question links saved');
        });
        // fs.writeFileSync( program.path + '/' + category[j] + '/status.json', JSON.stringify(check_links), 'utf8', function(err){
        //         if (err) throw err;
        //       console.log('Backup saved');
        // });
    }
    //yield nightmare.end();
};
q(run)(function(err) {
 if(err) throw err;
 console.log('done');
});
// setInterval(function() {q(run)(function(err) {
//  if(err) throw err;
//  console.log('done');
// });},43200);
//update per day 43200
