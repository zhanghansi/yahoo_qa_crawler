var _ = require('lodash');
var q = require('vo');
var fs= require('fs');
var Nightmare = require('nightmare');
var nightmare = Nightmare({
  show: false,
  gotoTimeout:  10000, // in ms
  loadTimeout:  10000,
  executionTimeout: 15000 // in ms
});

var program = require('commander');
program
    .option('-p, --path <path>', 'data export path') // ./../../yahoo_qa/
    .parse(process.argv);

var md5 = require('js-md5');
var root_path = '/mnt/data2/hansi.zhang/yahoo_qa_crawler/'
var cnt = 0;
var category = [  "cancer",
                  "alternative_medicine",
                  "dental",
                  "diet_fitness",
                  "allergies",
                  "diabetes",
                  "heart_diseases",
                  "infectious_diseases",
                  "other_diseases",
                  "respiratory_diseases",
                  "stds",
                  "skin_conditions",
                  "first_aid",
                  "injuries",
                  "other_general_health_care",
                  "pain",
                  "men_health",
                  "mental_health",
                  "women_health",
                  "other_health",
                  "optical"
                ];

var run = function * () {
    var backup_ctg = JSON.parse(fs.readFileSync(root_path + 'update_state.json', 'utf8'));
    console.log('start to update...')
    for (var k = category.indexOf(backup_ctg['ctg']); k < category.length; k++) {
        console.log('current category: ' + category[k]);

        /*** backup current category ***/
        var current_ctg = {};
        current_ctg['ctg'] = category[k];
        fs.writeFileSync(root_path + 'update_state.json', JSON.stringify(current_ctg), 'utf8');

        /*** prepare the cuurent_state for each category ***/
        var backup_state = JSON.parse(fs.readFileSync(program.path + '/' + current_ctg['ctg'] + '/status.json', 'utf8'));
        var current_state = JSON.parse(fs.readFileSync(program.path + '/' + current_ctg['ctg'] + '/status.json', 'utf8'));
        var update_links = JSON.parse(fs.readFileSync(program.path + '/' + current_ctg['ctg'] + '/question/questions.json', 'utf8'));
        console.log('total update length : ' + update_links.length);
        console.log('update left : ' + current_state.length);
        var today = new Date()
        // process.exit();

        // start to crawl
        for(var i = 0; i < current_state.length; i++) {
            console.log('current index : ' + i);
            console.log('current link: ' + current_state[i]);
        /***   Backup strategy for each category ***/
            var index = backup_state.indexOf(current_state[i]);
            backup_state.splice(index,1)
            if(backup_state.length == 0) {
                fs.writeFileSync(program.path + '/' + current_ctg['ctg'] + '/status.json', JSON.stringify(update_links), 'utf8');
            } else {
                fs.writeFileSync(program.path + '/' + current_ctg['ctg'] + '/status.json', JSON.stringify(backup_state), 'utf8');
            }
            if(current_state[i] == null) {
                console.log('link does not exist');
                continue;
            }

            var code = md5(current_state[i]);
            var path = program.path + '/' + current_ctg['ctg'] + '/detail/' + code + '.json';
            var exist = false;
            var changed = false;
            var old_version = {};
               //console.log(fs.existsSync('./'+program.update+'/detail/'+ code +'.json'));

            fs.open(path, 'r', (err,fd) => {
                if (err) {
                    if (err.code === "ENOENT") {
                      console.log('old_version does not exist');
                      return;
                    } else {
                      throw err;
                    }
                } else {
                    /***    Upload strategy     ***/
                    old_version = JSON.parse(fs.readFileSync(path, 'utf8'));
                    console.log('old_version exist');
                    exist = true;
                    days = Math.round((today.getTime() - Date.parse(old_version['last_update_date'])) / (1000 * 60 * 60 * 24))
                    console.log('days after last update : ' + days);
                    if (days > 90) {
                        var id = update_links.indexOf(current_state[i]);
                        update_links.splice(id,1)
                        fs.writeFileSync(program.path + '/' + current_ctg['ctg'] + '/question/questions.json', JSON.stringify(update_links), 'utf8');
                        fs.writeFileSync(program.path + '/' + current_ctg['ctg'] + '/upload/' + code +'.json', JSON.stringify(old_version), 'utf8');
                        fs.unlinkSync(program.path + '/' + current_ctg['ctg'] + '/detail/' + code + '.json');
                    }
                }
            });


            var author_link = "";
            var question = "";
            var question_content = "";
            var followers = 0;
            var number_of_answers = 0;
            var categories = [];
            var answers = [];
            var author_info = {};
            var answerSet = {};
            yield nightmare.useragent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36")
                .goto(current_state[i])
                .refresh()
                .inject('js',root_path + 'node_modules/jquery/dist/jquery.js')
                .wait(5000)
                .exists('#ya-question-breadcrumb #brdCrb > a')
                .then(function(success) {
                    if(!success) {
                        console.log('failed');
                        nightmare.refresh()
                                 .inject('js', root_path + 'node_modules/jquery/dist/jquery.js')
                                 .wait(5000);
                    }
                });
	    console.log("open the web page");
    /*** get basic info of question***/
            author_link = yield nightmare.evaluate(function() {
                if($('#ya-question-detail #yq-question-detail-profile-img  > a').length != 0) {
                    return $("#ya-question-detail #yq-question-detail-profile-img  > a").prop('href');
                } else {
                    return "Anonymous"
                }
            });
            console.log('author_link :'+author_link);
            // process.exit();
            question = yield nightmare.evaluate(function() {
                return $('#ya-question-detail > div').find("[itemprop=name]").text();
            });
            if(question == '') {
                continue;
            }
            question_content = yield nightmare.evaluate(function() {
                return $('#ya-question-detail > div').find("[itemprop=text]").text();
            });
            followers = yield nightmare.evaluate(function() {
                if($('#ya-question-detail .follow-text').text().split(" ")[0] == 'Follow') {
                    return 0;
                } else {
                    return parseInt($('#ya-question-detail .follow-text').text().split(" ")[0])
                }
            });
            categories = yield nightmare.evaluate(function() {
                var categorie = [];
                $('#ya-question-breadcrumb #brdCrb > a').each(function() {
                    categorie.push($(this).text());
                });
                return categorie;
            });
            number_of_answers = yield nightmare.evaluate(function() {
                if($('[itemprop=answerCount]').length != 0) {
                    return parseInt($('[itemprop=answerCount]').text());
                } else {
                    return 0;
                }
            });
            if(exist == true) {
                if(old_version['number_of_answers'] != number_of_answers) {
                    changed = true;
                }
            }
            if(exist == true && changed == false) {
                console.log('old_version does not changed');
                continue;
            }
            console.log('updated');
            if (number_of_answers != 0) {
                // do sth to collect answerSet
                var best_answer = yield nightmare.evaluate(function() {
                    var answer_set = {};
                    // check best answer
                    if($("#ya-best-answer").length != 0) {
                        answer_set['author_link'] = $("#ya-best-answer .uname").prop('href');
                        answer_set['author_name'] = $("#ya-best-answer .uname").text();
                        answer_set['timestamp'] = $("#ya-best-answer .ya-localtime").prop('title');
                        answer_set['likes'] = parseInt($("#ya-best-answer .tupwrap .count").text());
                        answer_set['dislikes'] = parseInt($("#ya-best-answer .tdownwrap .count").text());
                        answer_set['answer_content'] = $("#ya-best-answer .ya-q-full-text").text();
                        answer_set['best'] = true;
                    }
                    return answer_set;
                });
                if (best_answer['author_link'] != null) {
                    answers.push(best_answer);
                }
                var page = yield nightmare.evaluate(function() {
                    var page = 0;
                    if($('#ya-qn-pagination').length == 0) {
                        page = 1;
                    } else {
                        page = ($('#ya-qn-pagination a').length - 1)
                    }
                    return page;
                });
                for (var j = 0; j < page; j++) {
                    yield nightmare.inject('js', root_path + 'node_modules/jquery/dist/jquery.js');
                    var normal_answer = yield nightmare.evaluate(function() {
                        var answer_set = [];
                        $('#ya-qn-answers > li').each(function() {
                            var answer = {};
                            answer['author_link'] = $(this).find('.uname').prop('href');
                            answer['author_name'] = $(this).find('.uname').text();
                            answer['timestamp'] = $(this).find('.ya-localtime').prop('title')
                            answer['likes'] = parseInt($(this).find('.tupwrap .count').text());
                            answer['dislikes'] = parseInt($(this).find('.tdownwrap .count').text());
                            answer['answer_content'] = $(this).find('.ya-q-full-text').text();
                            answer['best'] = false;
                            answer_set.push(answer);
                        });
                        return answer_set;
                    });
                    answers = answers.concat(normal_answer);
                    if (j < page - 1) {
                        yield nightmare.click('#ya-qn-pagination a:last-child')
                                        .wait(3000);
                    }
                }
            }

            if(author_link != 'Anonymous') {
                var public = false;
                var name = '';
                var level = 0;
                var uid = author_link.substring(50,76);
                yield nightmare.click('#ya-question-detail #yq-question-detail-profile-img  > a')
                              .inject('js',root_path + 'node_modules/jquery/dist/jquery.js')
                              .wait(2000)
                              .exists('#member-details #name-text-long')
                              .then(function(success) {
                                if(!success) {
                                    nightmare.refresh()
                                             .inject('js',root_path + 'node_modules/jquery/dist/jquery.js')
                                             .wait(2000);
                                }
                              });
                public = yield nightmare.evaluate(function() {
                    return ($('#ya-myquestions .Bfc').length == 0);
                });
                name = yield nightmare.evaluate(function() {
                    return $('#member-details #name-text-long').text();
                });
                if(name == '') {
                    continue;
                }
                level = yield nightmare.evaluate(function() {
                    return parseInt($('#member-details #level-text p').text().split(' ')[1]);
                });
                question_general = yield nightmare.evaluate(function() {
                    var details = [];
                    $('#yan-mystatus tr > td').each(function(index) {
                        details[index] = $(this).find('.stat-count').text();
                    });
                    return details;
                });
                //console.log(question_general.length);
                author_info['author_link'] = author_link;
                author_info['uid'] = uid;
                author_info['public'] = public;
                author_info['name'] = name;
                author_info['level'] = level;
                author_info['points'] = parseInt(question_general[0].replace(/\D/g,''));
                author_info['best_answers(rate)'] = question_general[1];
                author_info['num_answers'] = parseInt(question_general[2].replace(/\D/g,''));
                author_info['num_questions'] = parseInt(question_general[3].replace(/\D/g,''));
            }
            // console.log(author_info);
            answerSet['url'] = current_state[i];
            answerSet['question'] = question;
            answerSet['question_content'] = question_content;
            answerSet['question_timestamp'] = current_state[i].split('=')[1].substring(0,14)
            answerSet['followers'] = followers;
            answerSet['number_of_answers'] = number_of_answers;
            answerSet['categories'] = categories;
            answerSet['answers'] = answers;
            answerSet['authorInfo'] = author_info;
            answerSet['last_update_date'] = today;
            // console.log(answerSet['last_update_date'].getMonth());
            fs.writeFile(program.path + '/' + current_ctg['ctg'] + '/detail/' + code + '.json', JSON.stringify(answerSet), 'utf8', function(err){
                            if (err) throw err;
                        });
        }


        // update all and reset to first category
        if(current_ctg['ctg'] == 'optical') {
            current_ctg['ctg'] = 'cancer';
            k = -1;
            fs.writeFileSync(root_path + 'update_state.json', JSON.stringify(current_ctg), 'utf8');
        }
    }
    console.log('finished');
    // yield nightmare.end();
}

q(run)(function(err) {
  if(err) throw err;
});
// setInterval(function() {q(run)(function(err) {
//   if(err) throw err;
//   console.log('done');
// });},604800);
// update per week
