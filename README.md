# Project Title

Yahoo Answer continues crawler

## Description 

This proejct uses nightmarejs to crawl yahoo_qa web pages. This example is crawling health related quesitons. But you can change it to other categories.
Make sure before you runing the program, build up the data folder, wihch includes question folder, detai folder, upload folder and  status.json.

## Getting Started

This project can be setup on linux. After download, run command "npm install"

### Prerequisites

forever : keep the crawler continuesly working
"sudo npm install -g forever"

xvfb : provide visual electron
sudo apt-get install xvfb


## Running the tests

1) continues crawl question links (health related)
bash batch_q.sh 

2) continues update question details
bash batch_u.sh


## Deployment

Add additional notes about how to deploy this on a live system


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Hansi Zhang** - *Jiang Bian*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc











