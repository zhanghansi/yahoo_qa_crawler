var program = require('commander');
program
    .option('-u, --update <update>', 'update new answers and comments')
    .parse(process.argv);

var forever = require('forever-monitor');
var child = new (forever.Monitor )('continues_crawl_update.js', {
  //options : options
  silent: true,
  args: ['-u', program.update],
  outFile: './out.log'
} );

//These events not required, but I like to hear about it.
child.on( "exit", function() {
  console.log( 'continues_crawl_update.js has exited!' );
} );
child.on( "restart", function() {
  console.log( 'continues_crawl_update.js has restarted.' );
} );
child.on( 'watch:restart', function( info ) {
  console.error( 'Restarting script because ' + info.file + ' changed' );
} );

//These lines actually kicks things off
child.start();

//You can catch other signals too
process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down \'node forever\' from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit();
} );

process.on( 'exit', function() {
  console.log( 'About to exit \'node forever\' process.' );
} );

//Sometimes it helps...
process.on( 'uncaughtException', function( err ) {
  console.log( 'Caught exception in \'node forever\': ' + err );
} );