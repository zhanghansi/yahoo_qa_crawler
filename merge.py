#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
import logging.handlers

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')

import os
import re
import json
import sys
import time
import csv
def extract_links(raw_json_data_folder):
    question_links = []
    logger.info(raw_json_data_folder)
    for root, dirs, files in os.walk(os.path.abspath(raw_json_data_folder)):
        for f in files:
            if not f.startswith('.'):
                logger.info(os.path.join(root, f))

                with open(os.path.join(root, f), 'r') as json_file:

                    data = json.load(json_file)
                    # for line in json_file:
                    question_links.append(data['url'])
    return question_links

def read_json(path):
    with open(path, 'r') as json_file:
        return json.load(json_file)

if __name__ == "__main__":
    # step1 merge upload file question links
    upload_question_links = extract_links('../../yahoo_qa/cancer/upload/')
    logger.info(len(upload_question_links))
    #step2 extract with question original
    original_links = read_json('../../yahoo_qa/cancer/question/questions.json')
    logger.info(len(original_links))
    # step3 merge
    result = list(set(upload_question_links + original_links))
    logger.info(len(result))
    with open('../../yahoo_qa/cancer/question/questions1.json', 'w') as outfile:
        json.dump(result, outfile)
